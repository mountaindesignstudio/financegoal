//
//  DatePickerVC.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/22/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class DatePickerVC: IterableVC {

    @IBOutlet weak var counterContainerView: UIView!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var titleTextLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    lazy var doneButton: UIBarButtonItem = { [unowned self] in
        return UIBarButtonItem(title: "DONE".localized, style: .done, target: self, action: #selector(IterableVC.didPressNextButton))
        }()
    
    override var _counterContainerView: UIView! {
        return counterContainerView
    }
    
    override var _counterLabel: UILabel! {
        return counterLabel
    }
    
    override var _titleTextLabel: UILabel! {
        return titleTextLabel
    }
    
    override var _descriptionLabel: UILabel! {
        return descriptionLabel
    }
    
    init() {
        super.init(nibName: "DatePickerVC", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func updateData() {
        super.updateData()
        
        self.datePicker.minimumDate = Date()
        self.datePicker.date = vm?.date ?? Date()
        
        updateNextButton()
    }
    
    override func updateBarButtons() {
        navigationItem.rightBarButtonItem = doneButton
    }
    
    override func updateNextButton() {
        doneButton.isEnabled = datePicker.date != Date()
    }
    
    @IBAction func datePickerAction(_ sender: Any) {
        vm?.date = datePicker.date
    }
}

extension DatePickerVC {
    
    var vm: DateViewModel? {
        return viewModel as? DateViewModel
    }
}
