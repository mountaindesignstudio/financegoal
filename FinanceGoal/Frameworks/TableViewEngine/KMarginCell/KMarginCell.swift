//
//  KMarginCell.swift
//  FinanceGoals
//
//  Created by Ilya Bondarenko on 11/1/18.
//  Copyright © 2018 Ilya Bondarenko. All rights reserved.
//

import UIKit

class KMarginCell: KTableCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundView?.backgroundColor = .clear
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
    }
    
    override func configure(cellModel: KCellModel!) {
        super.configure(cellModel: cellModel)
    }
}
