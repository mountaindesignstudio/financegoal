//
//  CreateSavingsVCCommand.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 2/11/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class CreateSavingsVCCommand: NSObject, KCellCommandProtocol {

    private(set) weak var delegate: CreateSavingsVCDelegate?
    
    // MARK: - Init method
    
    init(delegate: CreateSavingsVCDelegate?) {
        super.init()
        
        self.delegate = delegate
    }
}

class TypeAmountCommand: CreateSavingsVCCommand {
    
    func execute(withValue value: Any?) {
        guard let amountValue = value as? String else { return }
        delegate?.didChange(amountValue: amountValue)
    }
}

class ChangeGoalCommand: CreateSavingsVCCommand {
    
    func execute(withValue value: Any?) {
        delegate?.didPressChangeGoalButton()
    }
}
