//
//  MainScenario.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 12/25/18.
//  Copyright © 2018 Ilya Bondarenko. All rights reserved.
//

import UIKit

class MainScenario: NSObject, ScenarioInterface {

    /// Main window in application
    private(set) var window: UIWindow?
    
    /// Root tab bar controller
    private(set) var tabBarVC: UITabBarController?
    
    // MARK: - Scenarios
    
    var goalsListScenario: GoalsListScenario?
    var notificationsListScenario: NotificationsListScenario?
    var settingsScenario: SettingsScenario?
    
    // MARK: - Init method
    
    init(window: UIWindow?) {
        super.init()
        
        self.window = window
    }
    
    // MARK: - Scenario Interface methods
    
    func start() {
        createScenarios()
        showTabBarVC()
        startScenarios()
    }
    
    /// This method create tab bar controller and set it as like root view controller
    private func showTabBarVC() {
        tabBarVC = UITabBarController()
        tabBarVC?.tabBar.tintColor = Colors.tint
        tabBarVC?.tabBar.barTintColor = .white
        tabBarVC?.setViewControllers(viewControllers, animated: false)
        window?.rootViewController = tabBarVC
        window?.makeKeyAndVisible()
    }
}

extension MainScenario {
    
    func createScenarios() {
        goalsListScenario = GoalsListScenario()
        notificationsListScenario = NotificationsListScenario()
        settingsScenario = SettingsScenario()
    }
    
    func startScenarios() {
        goalsListScenario?.start()
        notificationsListScenario?.start()
        settingsScenario?.start()
    }
    
    var viewControllers: [UIViewController]! {
        var vcs = [UIViewController]()
        
        if let goalsListNVC = goalsListScenario?.nvc {
            vcs.append(goalsListNVC)
        }
        
        if let notificationsListNVC = notificationsListScenario?.nvc {
            vcs.append(notificationsListNVC)
        }
        
        if let settingsNVC = settingsScenario?.nvc {
            vcs.append(settingsNVC)
        }
        
        return vcs
    }
}
