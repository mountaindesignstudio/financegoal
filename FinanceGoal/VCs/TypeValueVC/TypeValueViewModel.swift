//
//  TypeValueViewModel.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/11/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class TypeValueViewModel: NSObject {

    var countValue: String! = "1"
    var titleValue: String! = ""
    var descriptionValue: String! = ""
    var placeholder: String! = ""
    var navBarTitle: String! = ""
    var keyboardType: UIKeyboardType! = .default
    
    var value: String! = ""
}

class PhotoViewModel: TypeValueViewModel {
    
    var image: UIImage?
    var imageURL: URL?
}

class DateViewModel: TypeValueViewModel {
    
    var date: Date? = Date()
}
