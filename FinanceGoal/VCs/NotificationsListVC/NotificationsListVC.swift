//
//  NotificationsListVC.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 12/26/18.
//  Copyright © 2018 Ilya Bondarenko. All rights reserved.
//

import UIKit

protocol NotificationsListVCDelegate: class {
    
}

class NotificationsListVC: UIViewController {

    var delegate: NotificationsListVCDelegate?
    
    // MARK: - UI
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // MARK: - Init method
    
    init() {
        super.init(nibName: "NotificationsListVC", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        descriptionLabel.text = "ENABLE_NOTIFICATIONS_DESCRIPTION".localized
        descriptionLabel.sizeToFit()
    }
}
