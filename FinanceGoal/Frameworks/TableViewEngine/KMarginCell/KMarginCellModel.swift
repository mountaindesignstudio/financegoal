//
//  MarginCellModel.swift
//  FinanceGoals
//
//  Created by Ilya Bondarenko on 11/1/18.
//  Copyright © 2018 Ilya Bondarenko. All rights reserved.
//

import UIKit

public let marginTableCell = "KMarginCell"

class KMarginCellModel: KCellModel {

    init(height: CGFloat!) {
        super.init(cellID: marginTableCell, height: height)
        update(separatorInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: UIScreen.main.bounds.width))
    }
}
