//
//  Application+Tools.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/24/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}
