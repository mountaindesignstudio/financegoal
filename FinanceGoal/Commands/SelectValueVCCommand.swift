//
//  SelectValueVCCommand.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 2/14/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class SelectValueVCCommand: NSObject, KCellCommandProtocol {

    private(set) weak var delegate: SelectValueVCDelegate?
    
    // MARK: - Init method
    
    init(delegate: SelectValueVCDelegate?) {
        super.init()
        
        self.delegate = delegate
    }
}

class SelectCommand: SelectValueVCCommand {
    
    func execute(withValue value: Any?) {
        delegate?.didSelect(value: value)
    }
}
