//
//  Blocks.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/22/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import Foundation

/// Completion block which contain array of goals.
typealias GoalsCompletion = (([Goal]) -> Void)?
