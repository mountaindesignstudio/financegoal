//
//  IterableVC.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/21/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

protocol IterableVCDelegate: class {
    func didPressCancelButton(typeValueVC: IterableVC!)
    
    func didPressNextButton(typeValueVC: IterableVC!)
    
    func didPressSkipButton(typeValueVC: IterableVC!)
}

class IterableVC: UIViewController {
    
    weak var delegate: IterableVCDelegate?
    
    private(set) var _counterContainerView: UIView!
    private(set) var _counterLabel: UILabel!
    private(set) var _titleTextLabel: UILabel!
    private(set) var _descriptionLabel: UILabel!
    
    var viewModel: TypeValueViewModel?
    
    lazy var cancelButton: UIBarButtonItem = { [unowned self] in
        return UIBarButtonItem(title: "CANCEL".localized, style: .plain, target: self, action: #selector(IterableVC.didPressCancelButton))
        }()
    
    lazy var nextButton: UIBarButtonItem = { [unowned self] in
        return UIBarButtonItem(title: "NEXT".localized, style: .done, target: self, action: #selector(IterableVC.didPressNextButton))
        }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        updateData()
        updateBarButtons()
    }
    
    func setupViews() {
        _counterContainerView.layer.cornerRadius = _counterContainerView.frame.width/2
    }
    
    func updateData() {
        title = viewModel?.navBarTitle
        
        _titleTextLabel.text = viewModel?.titleValue
        _counterLabel.text = viewModel?.countValue
        
        _descriptionLabel.text = viewModel?.descriptionValue
        _descriptionLabel.sizeToFit()
    }
    
    func updateBarButtons() {
        if navigationController?.viewControllers.first == self {
            navigationItem.leftBarButtonItem = cancelButton
        }
        navigationItem.rightBarButtonItem = nextButton
        navigationController?.navigationBar.tintColor = Colors.tint
    }
    
    func updateNextButton() {
        
    }
}

extension IterableVC {
    
    @objc func didPressCancelButton() {
        self.view.endEditing(true)
        delegate?.didPressCancelButton(typeValueVC: self)
    }
    
    @objc func didPressNextButton() {
        delegate?.didPressNextButton(typeValueVC: self)
    }
}
