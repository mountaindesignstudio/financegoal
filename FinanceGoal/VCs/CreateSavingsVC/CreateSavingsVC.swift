//
//  CreateSavingsVC.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/29/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

protocol CreateSavingsVCDelegate: class {
    
    /// This methid shoud be calling when the CreateSavingsVC did load
    func viewDidLoad(createSavingsVC: CreateSavingsVC!)
    
    /// This method must be calling when user press 'Cancel' button
    func didPressCancelButton(createSavingsVC: CreateSavingsVC!)
    
    /// This method must be calling when user changing an amount value text
    func didChange(amountValue: String!)
    
    /// This method must be calling when user tap segmented control's tabs
    func didChange(transactionType: TransactionType!)
    
    /// This method must be calling when user tap the Goal cell
    func didPressChangeGoalButton()
    
    /// This method must be calling when user tap 'Save' or 'Take' button
    func didPressCompleteButton()
}

class CreateSavingsVC: KTableView {

    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var _tableView: UITableView!
    @IBOutlet weak var _tableViewBottomMargin: NSLayoutConstraint!
    @IBOutlet weak var _buttonBottomMargin: NSLayoutConstraint!
    @IBOutlet weak var blurBottomView: UIView!
    @IBOutlet weak var completeButton: StandardButton!
    
    override var tableView: UITableView! {
        return _tableView
    }
    
    override var tableViewBottomMargin: NSLayoutConstraint? {
        return _tableViewBottomMargin
    }
    
    var delegate: CreateSavingsVCDelegate?
    
    lazy var cancelButton: UIBarButtonItem = { [unowned self] in
        return UIBarButtonItem(title: "CANCEL".localized, style: .plain, target: self, action: #selector(CreateSavingsVC.didPressCancelButton))
        }()
    
    // MARK: - Init method
    
    init() {
        super.init(nibName: "CreateSavingsVC", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        delegate?.viewDidLoad(createSavingsVC: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarView?.backgroundColor = navigationController?.navigationBar.barTintColor
        navigationController?.navigationBar.hideBorderLine()
        navigationController?.navigationBar.tintColor = Colors.tint
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func update(viewModel: KViewModel!) {
        super.update(viewModel: viewModel)
        
        guard let vm = vm else { return }
        completeButton?.setTitle(vm.completeButtonTitleValue, for: .normal)
        completeButton?.isEnabled = vm.isEnableCompleteButton
        segmentController.selectedSegmentIndex = vm.transactionType.rawValue
    }
    
    override func keyboardWillHide() {
        changeBottomMarginValue(withDuration: 0.3, value: 0.0)
    }
    
    override func keyboardFrameWillChange(notification: NSNotification) {
        let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height
        let duration = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double)
        changeBottomMarginValue(withDuration: duration ?? 0.1, value: (keyboardHeight ?? 0.0) - SafeArea.bottom)
    }
}

/// The extension for setup views on this view controller
extension CreateSavingsVC: UIToolbarDelegate {
    
    private func setupViews() {
        toolbar.delegate = self
        blurBottomView.addBlur(.light)
        tableView.tableFooterView = UIView()
        navigationItem.leftBarButtonItem = cancelButton
        
        setupSegmentControl()
    }
    
    private func setupSegmentControl() {
        segmentController?.removeAllSegments()
        segmentController.insertSegment(withTitle: "SAVE".localized, at: 0, animated: false)
        segmentController.insertSegment(withTitle: "TAKE".localized, at: 1, animated: false)
    }
    
    func positionForBar(bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
}

/// The extension for work with actions
extension CreateSavingsVC {
    
    @objc func didPressCancelButton() {
        self.view.endEditing(true)
        delegate?.didPressCancelButton(createSavingsVC: self)
    }
    
    @IBAction func didChangeSegmentedControlValue(_ sender: Any) {
        delegate?.didChange(transactionType: TransactionType(rawValue: segmentController.selectedSegmentIndex))
    }
    
    @IBAction func completeButtonAction(_ sender: Any) {
        delegate?.didPressCompleteButton()
    }
}

extension CreateSavingsVC {
    
    /// This method change the bottom margin value with animation
    ///
    /// - Parameters:
    ///     - duration: the time interval value for animation
    ///     - value: the new value for the bottom margin
    fileprivate func changeBottomMarginValue(withDuration duration: TimeInterval, value: CGFloat) {
        UIView.animate(withDuration: duration) {
            self._buttonBottomMargin.constant = -value
            self._tableViewBottomMargin.constant = value
        }
    }
}

extension CreateSavingsVC {
    var vm: CreateSavingsVM? {
        return viewModel as? CreateSavingsVM
    }
}

/// The enum with available transactions type
enum TransactionType: Int {
    case save = 0
    case take = 1
}
