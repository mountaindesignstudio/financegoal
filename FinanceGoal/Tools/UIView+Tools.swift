//
//  UIView+Tools.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/23/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func addShadow(offsetHeight: CGFloat = 3.0) {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: offsetHeight)
        layer.shadowOpacity = 0.2
        layer.shadowRadius = 8.0
    }
    
    func addBlur(_ style: UIBlurEffect.Style!) {
        
        if !UIAccessibility.isReduceTransparencyEnabled {
            
            if style == .light || style == .extraLight {
                let bgView = UIView.init(frame: self.bounds)
                bgView.backgroundColor = .white//UIColor.white
                bgView.layer.opacity = 0.9
                self.addSubview(bgView)
            }
            
            let blurEffect = UIBlurEffect(style: style)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            //always fill the view
            blurEffectView.frame = self.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            self.addSubview(blurEffectView) //if you have more UIViews, use an insertSubview API to place it where needed
        } else {
            self.backgroundColor = UIColor.white
        }
    }
}

