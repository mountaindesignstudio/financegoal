//
//  Goal+Common.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 2/13/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import Foundation

extension Goal {
    
    /// This property return sum of savings amount for current Goal object
    var savingsAmount: Float {
        
        guard let transactions = self.transactions else { return 0.0 }
        
        var sum: Int64! = 0
        
        for transaction in transactions {
            if let t = transaction as? Transaction, let type = TransactionType(rawValue: Int(t.type)) {
                if type == .save {
                    sum += t.amount
                } else if type == .take {
                    sum -= t.amount
                }
            }
        }
        
        return Float(sum)
    }
    
    /// This property return current progress in percent.
    /// This value can be equal 0...1
    var progress: Float {
        let progress = savingsAmount/amount
        
        if progress < 0.0 {
            return 0.0
        } else if progress > 1.0 {
            return 1.0
        } else {
            return progress
        }
    }
    
    /// This property return current left amount value for current goal
    var leftAmount: Float {
        let amount = Int64(self.amount)
        let savings = Int64(self.savingsAmount)
        let value = Float(amount - savings)
        
        if value < 0 { return 0.0 }
        
        return value
    }
}

extension Goal {
    
    var isLessThanYear: Bool {
        let newDate = Calendar.current.date(byAdding: .year, value: 1, to: startDate!)
        return newDate! <= expirationDate!
    }
    
    var isLessThanMonth: Bool {
        let newDate = Calendar.current.date(byAdding: .month, value: 1, to: startDate!)
        return newDate! <= expirationDate!
    }
    
    var isLessThanWeek: Bool {
        let newDate = Calendar.current.date(byAdding: .day, value: 7, to: startDate!)
        return newDate! <= expirationDate!
    }
}

extension Goal {
    
    func fetchHistory(completion: GoalsCompletion) {
        var transactions = [Transaction]()
        
        if let trns = self.transactions {
            for transaction in trns {
                if let tr = transaction as? Transaction {
                    transactions.append(tr)
                }
            }
        }
        
        let currentDate = Date()
        var count = 0
        
        if isLessThanMonth {
            count = 4
        } else {
            count = (expirationDate?.months(from: currentDate)) ?? 0
        }
    }
}
