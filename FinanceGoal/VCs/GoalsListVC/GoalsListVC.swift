//
//  GoalsListVC.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 12/26/18.
//  Copyright © 2018 Ilya Bondarenko. All rights reserved.
//

import UIKit
//import TableViewEngine

protocol GoalsListVCDelegate: class {
    
    /// This method should be calling when user tap the create goal button
    func didPressCreateGoalButton(goalListVC: GoalsListVC!)
    
    /// This methid shoud be calling when the Goal List vc did load
    func viewDidLoad(goalListVC: GoalsListVC!)
    
    /// This method should be calling when user select the goal from list
    func didSelect(goal: Goal!)
    
    /// This method should be calling when user press Save Money button
    func didPressSaveMoneyButton(goalListVC: GoalsListVC!)
}

class GoalsListVC: KTableView {

    var delegate: GoalsListVCDelegate?
    
    // MARK: - UI
    
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var navBarBackgorundHeight: NSLayoutConstraint!
    
    lazy var addGoalButton: UIButton = { [unowned self] in
        let rect = CGRect(x: 0, y: 0, width: 36.0, height: 36.0)
        let button = UIButton(frame: rect)
        button.setImage(#imageLiteral(resourceName: "add_icon"), for: .normal)
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(GoalsListVC.addGoalButtonAction(_:)),
                         for: UIControl.Event.touchUpInside)
        return button
    }()
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var _tableView: UITableView!
    
    @IBOutlet weak var saveMoneyContainer: UIView!
    @IBOutlet weak var saveMoneyButton: UIButton!
    
    // MARK: -
    
    override var tableView: UITableView! {
        return _tableView
    }
    
    // MARK: - Init method
    
    init() {
        super.init(nibName: "GoalsListVC", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        descriptionLabel.text = "ADD_GOAL_DESCRIPTION".localized
        descriptionLabel.sizeToFit()
        saveMoneyButton?.setTitle("   " + "SAVE_MONEY".localized, for: .normal)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: UIScreen.main.bounds.width, bottom: 0, right: 0)
        tableView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 56.0, right: 0.0)
        setNeedsStatusBarAppearanceUpdate()
        
        delegate?.viewDidLoad(goalListVC: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.defaultSetup()
        navigationController?.navigationBar.prefersLargeTitles = true
        let statusBarHeight = UIApplication.shared.statusBarFrame.size.height
        navBarBackgorundHeight.constant = statusBarHeight + 44.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.add(button: addGoalButton)
        UIApplication.shared.statusBarView?.backgroundColor = .white
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        addGoalButton.removeFromSuperview()
    }
    
    override func update(viewModel: KViewModel!) {
        super.update(viewModel: viewModel)
        
        guard let vm = self.vm else { return }
        
        tableView.isHidden = vm.isEmpty
        emptyView.isHidden = !vm.isEmpty
        saveMoneyContainer.isHidden = vm.isEmpty
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Actions
    
    @IBAction func addGoalButtonAction(_ sender: Any) {
        delegate?.didPressCreateGoalButton(goalListVC: self)
    }
    
    @IBAction func saveMoneyButtonAction(_ sender: Any) {
        delegate?.didPressSaveMoneyButton(goalListVC: self)
    }
}

extension GoalsListVC {
    
    var vm: GoalListVM? {
        return viewModel as? GoalListVM
    }
}
