//
//  KSectionModel.swift
//  FinanceGoals
//
//  Created by Ilya Bondarenko on 10/26/18.
//  Copyright © 2018 Ilya Bondarenko. All rights reserved.
//

import UIKit

class KSectionModel: NSObject {

    /// Array of cell models for draw UITableViewCell for one section.
    private(set) var cellModels: [KCellModel]! = [KCellModel]()
    
    // MARK: - Init method
    
    init(cellModels: [KCellModel]!) {
        super.init()
        
        self.cellModels = cellModels
    }
    
    // MARK: - Tools
    
    func update(cellModels: [KCellModel]!) {
        self.cellModels = cellModels
    }
}
