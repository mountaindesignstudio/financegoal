//
//  TitleTableCell.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/30/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class TitleTableCell: KTableCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func configure(cellModel: KCellModel!) {
        super.configure(cellModel: cellModel)
        
        guard let cm = cm else { return }
        titleLabel.text = cm.titleValue.uppercased()
    }
}

extension TitleTableCell {
    
    var cm: TitleCellModel? {
        return cellModel as? TitleCellModel
    }
}
