//
//  GoalsListScenario.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 12/26/18.
//  Copyright © 2018 Ilya Bondarenko. All rights reserved.
//

import UIKit

class GoalsListScenario: TabBarItemScenario, ScenarioInterface {
    
    /// The scenario object for create a new goal
    var createGoalScenario: CreateGoalScenario?
    
    /// The scenario object for show goal details
    var goalDetailsScenario: GoalDetailsScenario?
    
    /// The scenario object for save/take money
    var createSavingsScenario: CreateSavingsScenario?
    
    /// The view controller for show list of goals
    private(set) var goalsListVC: GoalsListVC?
    
    // MARK: -
    
    override var titleValue: String! {
        return "GOALS".localized
    }
    
    // MARK: - Init method
    
    override init() {
        super.init()
        
        createGoalsListVC()
    }
    
    // MARK: - ScenarioInterface methods
    
    func start() {}
    
    func stop() {}
    
    // MARK: - Private tools
    
    private func createGoalsListVC() {
        goalsListVC = GoalsListVC()
        goalsListVC?.delegate = self
        createNavigationController(forViewController: goalsListVC!)
    }
    
    // MARK: - Tools
    
    func reloadData() {
        weak var weakSelf = self
        GoalManager.shared.fetchGoals { (goals) in
            let vm = GoalListVM(goals: goals, delegate: weakSelf)
            vm.commandDelegate = self
            weakSelf?.goalsListVC?.update(viewModel: vm)
        }
    }
}

extension GoalsListScenario: GoalsListVCDelegate {
    
    func viewDidLoad(goalListVC: GoalsListVC!) {
        reloadData()
    }
    
    func didPressCreateGoalButton(goalListVC: GoalsListVC!) {
        createGoalScenario = CreateGoalScenario(rootVC: goalListVC.navigationController, delegate: self)
        createGoalScenario?.start()
    }
    
    func didSelect(goal: Goal!) {
        guard let nvc = nvc else { return }
        goalDetailsScenario = GoalDetailsScenario(goal: goal, nvc: nvc)
        goalDetailsScenario?.start()
    }
    
    func didPressSaveMoneyButton(goalListVC: GoalsListVC!) {
        guard let tabBarVC = goalListVC?.tabBarController else { return }
        createSavingsScenario = CreateSavingsScenario(rootVC: tabBarVC)
        createSavingsScenario?.delegate = self
        createSavingsScenario?.start()
    }
}

extension GoalsListScenario: CreateGoalScenarioDelegate {
    
    func didComplete(createGoalScenario: CreateGoalScenario!) {
        createGoalScenario.stop()
        self.createGoalScenario = nil
        reloadData()
    }
    
    func didCancel(createGoalScenario: CreateGoalScenario!) {
        createGoalScenario.stop()
        self.createGoalScenario = nil
    }
}

extension GoalsListScenario: CreateSavingsScenarioDelegate {
    
    func didCompleteScenario(createSavingsScenario: CreateSavingsScenario!) {
        stopCreateSavingsScenario()
        reloadData()
    }
    
    func didCancelScenario(createSavingsScenario: CreateSavingsScenario!) {
        stopCreateSavingsScenario()
    }
    
    private func stopCreateSavingsScenario() {
        createSavingsScenario?.stop()
        createSavingsScenario = nil
    }
}
