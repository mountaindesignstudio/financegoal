//
//  NavigationScenario.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/11/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class NavigationScenario: NSObject {

    /// Root nvc for tab bar item scenario
    var nvc: UINavigationController?
    
    /// Title value for view controller
    private(set) var titleValue: String!
    
    // MARK: - Tools
    
    func createNavigationController(forViewController viewController: UIViewController!) {
        viewController?.title = titleValue
        nvc = UINavigationController(rootViewController: viewController!)
        nvc?.navigationBar.prefersLargeTitles = true
    }
}
