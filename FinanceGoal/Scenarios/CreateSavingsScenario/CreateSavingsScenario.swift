//
//  CreateSavingsScenario.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/29/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

protocol CreateSavingsScenarioDelegate: class {
    
    func didCompleteScenario(createSavingsScenario: CreateSavingsScenario!)
    
    func didCancelScenario(createSavingsScenario: CreateSavingsScenario!)
}

class CreateSavingsScenario: NavigationScenario, ScenarioInterface {

    /// Delegate object for callbacks
    weak var delegate: CreateSavingsScenarioDelegate?
    
    private(set) var rootVC: UIViewController!
    private(set) var createSavingsVC: CreateSavingsVC?
    private(set) var selectGoalVC: SelectValueVC?
    
    /// The current selected goal for add transaction for this goal
    private(set) var selectedGoal: Goal?
    
    // MARK: - Init method
    
    init(rootVC: UIViewController!, selectedGoal: Goal? = nil) {
        super.init()
        
        self.rootVC = rootVC
        self.selectedGoal = selectedGoal
    }
    
    // MARK: - ScenarioInterface methods
    
    func start() {
        showCreateSavingsVC()
    }
    
    func stop() {
        createSavingsVC?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Private Tools
    
    private func showCreateSavingsVC() {
        createSavingsVC = CreateSavingsVC()
        createSavingsVC?.delegate = self
        nvc = UINavigationController(rootViewController: createSavingsVC!)
        rootVC.present(nvc!, animated: true, completion: nil)
    }
    
    private func showSelectGoalVC() {
        selectGoalVC = SelectValueVC()
        selectGoalVC?.delegate = self
        nvc?.pushViewController(selectGoalVC!, animated: true)
    }
}

extension CreateSavingsScenario: CreateSavingsVCDelegate {
    
    func viewDidLoad(createSavingsVC: CreateSavingsVC!) {
        
        guard let goal = selectedGoal else {
            GoalManager.shared.fetchGoals { [weak self] (goals) in
                guard let goal = goals.first else { return }
                self?.selectedGoal = goal
                self?.reloadScreen(withGoal: goal, amount: nil, transactionType: self?.getCurrentTransactionType())
            }
            
            return
        }
        
        reloadScreen(withGoal: goal, amount: nil, transactionType: getCurrentTransactionType())
    }
    
    func didPressCancelButton(createSavingsVC: CreateSavingsVC!) {
        delegate?.didCancelScenario(createSavingsScenario: self)
    }
    
    func didChange(amountValue: String!) {
        reloadScreen(withGoal: selectedGoal!, amount: amountValue, transactionType: getCurrentTransactionType())
    }
    
    func didChange(transactionType: TransactionType!) {
        reloadScreen(withGoal: selectedGoal!, amount: getCurrentAmount(), transactionType: transactionType)
    }
    
    func didPressChangeGoalButton() {
        showSelectGoalVC()
    }
    
    func didPressCompleteButton() {
        guard let amount = getCurrentAmount(), let type = getCurrentTransactionType()?.rawValue else { return }
        
        let transaction = TransactionManager.shared.transaction(byTransactionID: String.generateID())
        transaction.amount = Int64(amount) ?? 0
        transaction.type = Int16(type)
        transaction.goal = selectedGoal
        transaction.date = Date()
        
        DataBaseManager.shared.saveContext()
        delegate?.didCompleteScenario(createSavingsScenario: self)
    }
}

extension CreateSavingsScenario {
    
    private func reloadScreen(withGoal goal: Goal, amount: String?, transactionType: TransactionType!) {
        let vm = CreateSavingsVM(goal: goal, amountValue: amount, cellDelegate: self, transactionType: transactionType)
        createSavingsVC?.update(viewModel: vm)
    }
    
    private func getCurrentTransactionType() -> TransactionType! {
        guard let transactionType = createSavingsVC?.vm?.transactionType else {
            return .save
        }
        return transactionType
    }
    
    private func getCurrentAmount() -> String? {
        return createSavingsVC?.vm?.amountValue
    }
}

extension CreateSavingsScenario: SelectValueVCDelegate {
    
    func viewDidLoad(selectValueVC: SelectValueVC!) {
        GoalManager.shared.fetchGoals { [weak self] (goals) in
            let vm = SelectGoalVM(goals: goals, delegate: self)
            self?.selectGoalVC?.update(viewModel: vm)
        }
    }
    
    func didSelect(value: Any?) {
        guard let goal = value as? Goal else { return }
        selectedGoal = goal
        reloadScreen(withGoal: goal, amount: getCurrentAmount(), transactionType: getCurrentTransactionType())
        nvc?.popViewController(animated: true)
    }
}
