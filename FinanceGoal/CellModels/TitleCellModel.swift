//
//  TitleCellModel.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/30/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class TitleCellModel: KCellModel {

    private(set) var titleValue: String! = ""
    
    init(titleValue: String!, cellID: CellID!, height: CGFloat) {
        super.init(cellID: cellID, height: height)
        
        self.titleValue = titleValue
        update(separatorInset: UIEdgeInsets(top: 0.0, left: UIScreen.main.bounds.width, bottom: 0.0, right: 0.0))
    }
}
