//
//  TypeValueVC.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/11/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class TypeValueVC: IterableVC {
    
    // MARK: - UI
    
    @IBOutlet weak var counterContainerView: UIView!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var titleTextLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var topSeparatorHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomSeparatorHeightConstant: NSLayoutConstraint!
    
    // MARK: - Override properties
    
    override var _counterContainerView: UIView! {
        return counterContainerView
    }
    
    override var _counterLabel: UILabel! {
        return counterLabel
    }
    
    override var _titleTextLabel: UILabel! {
        return titleTextLabel
    }
    
    override var _descriptionLabel: UILabel! {
        return descriptionLabel
    }
    
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.shared.statusBarView?.backgroundColor = navigationController?.navigationBar.barTintColor
        textField.becomeFirstResponder()
    }
    
    override func updateData() {
        super.updateData()
        
        textField.placeholder = viewModel?.placeholder
        textField.keyboardType = viewModel?.keyboardType ?? .default
        
        updateNextButton()
    }
    
    override func setupViews() {
        super.setupViews()
        topSeparatorHeightConstraint.constant = 0.3
        bottomSeparatorHeightConstant.constant = 0.5
        
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField.delegate = self
    }
    
    override func updateNextButton() {
        nextButton.isEnabled = !textField.isEmpty
    }
}

extension TypeValueVC: UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        viewModel?.value = textField.text ?? ""
        updateNextButton()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        delegate?.didPressNextButton(typeValueVC: self)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}

class TypeCostValueVC: TypeValueVC {
    
    init() {
        super.init(nibName: "TypeValueVC", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc override func updateData() {
        super.updateData()
        textField.text = viewModel!.value.priceWithoutCurrency
    }

    override func textFieldDidChange(_ textField: UITextField) {
        updateNextButton()
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch string {
        case "0","1","2","3","4","5","6","7","8","9":
            viewModel!.value += string
            textField.text = viewModel!.value.priceWithoutCurrency
        default:
            let array = Array(string)
            var currentStringArray = Array(viewModel!.value)
            if array.count == 0 && currentStringArray.count != 0 {
                currentStringArray.removeLast()
                viewModel?.value = ""
                for character in currentStringArray {
                    viewModel!.value += String(character)
                }
                
                textField.text = viewModel!.value.priceWithoutCurrency
            }
        }
        
        updateNextButton()
        
        return false
    }
}
