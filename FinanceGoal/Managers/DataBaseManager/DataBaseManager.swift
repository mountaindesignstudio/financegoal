//
//  DataBaseManager.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/22/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit
import CoreData

class DataBaseManager: NSObject {

    static let shared = DataBaseManager()
    
    // MARK: - Create and get core data info methods
    
    // Get application documetns directory method
    lazy var applicationDocumentsDirectory: NSURL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1] as NSURL
    }()
    
    // Get managed object model method
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "FinanceGoal", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    // Get managed object context method
    lazy var managedObjectContext: NSManagedObjectContext = {
        return persistentContainer.viewContext
    }()
    
    // Save context method
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    // MARK: - Tools
    
    /// Create new object with name
    func newEntity(name: String!) -> NSManagedObject {
        return NSEntityDescription.insertNewObject(forEntityName: name, into: self.managedObjectContext)
    }
    
    // Entity for Name
    func entityForName(entityName: String) -> NSEntityDescription {
        return NSEntityDescription.entity(forEntityName: entityName, in: self.managedObjectContext)!
    }
    
    /// Fetch entities with parameters
    func fetchEntities(forName name: String!, sortDescriptors: Array<NSSortDescriptor>? = nil, limit: Int? = nil, predicate: NSPredicate? = nil) -> [Any] {
        
        // Initialize Fetch Request
        var entityes  = [Any]()
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        if let sortDescriptorsValue = sortDescriptors { fetchRequest.sortDescriptors = sortDescriptorsValue }
        if let limitValue = limit { fetchRequest.fetchLimit = limitValue }
        if let predicateValue = predicate { fetchRequest.predicate = predicateValue }
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: name, in: self.managedObjectContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        
        do {
            entityes = try self.managedObjectContext.fetch(fetchRequest)
            return entityes
        } catch {
            let fetchError = error as NSError
            print(fetchError)
            return entityes
        }
    }
    
    /// Remove entity with parameters
    func removeEntity(predicate: NSPredicate!, name: String!) {
        
        let transactionEntities = fetchEntities(forName: name, predicate: predicate)
        
        do {
            for ent in transactionEntities {
                managedObjectContext.delete(ent as! NSManagedObject)
            }
            
            saveContext()
        }
    }
    
    /// Remove all entities for name
    func removeAllEntities(forName name: String!) {
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: name)
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        
        do {
            try managedObjectContext.execute(request)
            saveContext()
        } catch {
            print ("There was an error")
        }
    }
}
