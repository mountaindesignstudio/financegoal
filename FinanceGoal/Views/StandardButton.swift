//
//  StandardButton.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 12/29/18.
//  Copyright © 2018 Ilya Bondarenko. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class StandardButton: UIButton {
    
    @IBInspectable var cornerRadius: CGFloat = 8.0 {
        didSet {
            updateCorners(value: cornerRadius)
        }
    }
    
    @IBInspectable var text: String = "Button" {
        didSet {
            update(text: text)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    override func prepareForInterfaceBuilder() {
        sharedInit()
    }
    
    // MARK: - Private tools
    
    private func sharedInit() {
        updateCorners(value: cornerRadius)
        update(text: text)
    }
    
    private func updateCorners(value: CGFloat) {
        layer.cornerRadius = value
    }
    
    private func update(text: String!) {
        setTitle(text.localized, for: .normal)
    }
}
