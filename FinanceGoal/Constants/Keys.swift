//
//  Keys.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/22/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import Foundation

struct EntityName {
    static let goal = "Goal"
    static let transaction = "Transaction"
}

struct TableCellID {
    static let goalLargeCell: CellID = "GoalLargeTableCell"
    static let goalMediumTableCell: CellID = "GoalMediumTableCell"
    static let goalDetailsTableCell: CellID = "GoalDetailsTableCell"
    static let selectGoalTableCell: CellID = "SelectGoalTableCell"
    static let titleTableCell: CellID = "TitleTableCell"
    static let typeAmountTableCell: CellID = "TypeAmountTableCell"
}
