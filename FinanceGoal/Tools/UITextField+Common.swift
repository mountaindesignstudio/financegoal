//
//  UITextField+Common.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/14/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    var isEmpty: Bool {
        guard let text = text else { return true }
        return !(text.count > 0)
    }
}
