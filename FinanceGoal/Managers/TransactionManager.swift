//
//  TransactionManager.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 2/12/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class TransactionManager: NSObject {

    static let shared = TransactionManager()
    
    /// This method check if Transaction is exists by ID and return it.
    /// If transaction not exists the method returns new Transaction.
    func transaction(byTransactionID transactionID: String!) -> Transaction {
        let predicate = NSPredicate(format: "transactionID ==" + transactionID)
        if let transaction = DataBaseManager.shared.fetchEntities(forName: EntityName.transaction, limit: 1, predicate: predicate).first as? Transaction {
            return transaction
        } else {
            let transaction = DataBaseManager.shared.newEntity(name: EntityName.transaction) as! Transaction
            transaction.transactionID = transactionID
            return transaction
        }
    }
}
