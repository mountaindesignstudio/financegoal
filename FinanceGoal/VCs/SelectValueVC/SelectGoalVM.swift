//
//  SelectGoalVM.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 2/13/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class SelectGoalVM: KViewModel {
    
    private(set) var goals: [Goal]! = [Goal]()
    weak var cellDelegate: SelectValueVCDelegate?
    
    // MARK: - Init method
    
    init(goals: [Goal]!, delegate: SelectValueVCDelegate?) {
        super.init()
        
        self.goals = goals
        self.cellDelegate = delegate
        
        generateSection()
    }
    
    // MARK: - Private Tools
    
    private func generateSection() {
        
        var cellModels = [KCellModel]()
        cellModels.append(KMarginCellModel(height: 10.0))
        for goal in goals {
            let cmd = SelectCommand(delegate: cellDelegate)
            let cm = GoalCellModel(cellID: TableCellID.goalMediumTableCell, height: 90.0, goal: goal, command: cmd)
            cellModels.append(cm)
        }
        
        sections.append(KSectionModel(cellModels: cellModels))
    }
}
