//
//  ScenarioInterface.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 12/25/18.
//  Copyright © 2018 Ilya Bondarenko. All rights reserved.
//

import Foundation

@objc protocol ScenarioInterface {
    
    @objc optional func start()
    
    @objc optional func stop()
}
