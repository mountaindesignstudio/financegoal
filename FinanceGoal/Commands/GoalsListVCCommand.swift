//
//  GoalsListVCCommand.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/24/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class GoalsListVCCommand: NSObject, KCellCommandProtocol {

    private(set) weak var delegate: GoalsListVCDelegate?
    
    // MARK: - Init method
    
    init(delegate: GoalsListVCDelegate?) {
        super.init()
        
        self.delegate = delegate
    }
}

class SelectGoalCommand: GoalsListVCCommand {
    
    func execute(withValue value: Any?) {
        guard let goal = value as? Goal else { return }
        delegate?.didSelect(goal: goal)
    }
}

