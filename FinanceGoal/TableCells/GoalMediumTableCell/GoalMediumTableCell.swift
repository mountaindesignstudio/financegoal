//
//  GoalMediumTableCell.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 2/13/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class GoalMediumTableCell: KTableCell {

    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var percentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.layer.cornerRadius = 8.0
    }

    override func configure(cellModel: KCellModel!) {
        super.configure(cellModel: cellModel)
        
        titleLabel.text = cm?.title
        percentLabel.text = cm?.percentValue
        leftLabel.text = cm?.left
        
        photoView.sd_setImage(with: cm!.imageURL) { (image, error, type, url) in
            self.photoView.image = image
        }
    }
}

extension GoalMediumTableCell {
    
    var cm: GoalCellModel? {
        return cellModel as? GoalCellModel
    }
}
