//
//  CreateGoalScenario.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/11/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit
import SDWebImage

protocol CreateGoalScenarioDelegate: class {
    
    func didComplete(createGoalScenario: CreateGoalScenario!)
    
    func didCancel(createGoalScenario: CreateGoalScenario!)
}

class CreateGoalScenario: NavigationScenario, ScenarioInterface {

    /// The root view controller for present create goal vc
    private(set) var rootVC: UIViewController?
    
    private(set) var nameVC: TypeValueVC?
    private(set) var amountVC: TypeCostValueVC?
    private(set) var photoVC: PhotoVC?
    private(set) var datePickerVC: DatePickerVC?
    
    /// Delegate object for callback
    weak var delegate: CreateGoalScenarioDelegate?
    
    // MARK: - Init method
    
    init(rootVC: UIViewController?, delegate: CreateGoalScenarioDelegate?) {
        super.init()
        
        self.rootVC = rootVC
        self.delegate = delegate
    }
    
    // MARK: - ScenarioInterface methods
    
    func start() {
        showTypeNameVC()
    }
    
    func stop() {
        rootVC?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Private tools
    
    private func showTypeNameVC() {
        nameVC = TypeValueVC()
        nameVC?.delegate = self
        let vm = TypeValueViewModel()
        vm.navBarTitle = "CREATE_GOAL".localized
        vm.titleValue = "NAME".localized
        vm.placeholder = "ENTER_GOAL_NAME".localized
        vm.descriptionValue = ""
        vm.countValue = "1"
        nameVC?.viewModel = vm
        
        createNavigationController(forViewController: nameVC)
        nvc?.navigationBar.prefersLargeTitles = false
        
        rootVC?.present(nvc!, animated: true, completion: nil)
    }
    
    private func showTypeAmountVC() {
        amountVC = TypeCostValueVC()
        amountVC?.delegate = self
        let vm = TypeValueViewModel()
        vm.navBarTitle = "CREATE_GOAL".localized
        vm.titleValue = "COST".localized
        vm.placeholder = "ENTER_GOAL_COST".localized
        vm.descriptionValue = "GOAL_COST_DESCRIPTION".localized
        vm.countValue = "2"
        vm.keyboardType = UIKeyboardType.decimalPad
        amountVC?.viewModel = vm
        
        nameVC?.navigationController?.pushViewController(amountVC!, animated: true)
    }
    
    private func showPhotoVC() {
        photoVC = PhotoVC()
        photoVC?.delegate = self
        let vm = PhotoViewModel()
        vm.navBarTitle = "CREATE_GOAL".localized
        vm.titleValue = "GOAL_IMAGE".localized
        vm.descriptionValue = "GOAL_IMAGE_DESCRIPTION".localized
        vm.countValue = "3"
        photoVC?.viewModel = vm
        
        amountVC?.navigationController?.pushViewController(photoVC!, animated: true)
    }
    
    private func showDatePickerVC() {
        datePickerVC = DatePickerVC()
        datePickerVC?.delegate = self
        
        let vm = DateViewModel()
        vm.navBarTitle = "CREATE_GOAL".localized
        vm.titleValue = "EXPIRATION_DATE".localized
        vm.descriptionValue = "EXPIRATION_DATE_DESCRIPTION".localized
        vm.countValue = "4"
        datePickerVC?.viewModel = vm
        
        photoVC?.navigationController?.pushViewController(datePickerVC!, animated: true)
    }
    
    // MARK: - Create Goal method
    
    func createGoal() {
        guard let name = nameVC?.viewModel?.value,
            let amount = (amountVC?.viewModel?.value as NSString?)?.floatValue,
            let expirationDate = (datePickerVC?.viewModel as? DateViewModel)?.date else { return }
        
        let goal = GoalManager.shared.goal(byGoalID: String.generateID())
        goal.name = name
        goal.amount = amount
        goal.expirationDate = expirationDate
        
        if let image = (photoVC?.viewModel as? PhotoViewModel)?.image,
            let data = image.pngData(), let url = (photoVC?.viewModel as? PhotoViewModel)?.imageURL {
            SDWebImageManager.shared().saveImage(toCache: image, for: url)
            goal.image = data
            goal.imageURL = url
        }
        
        DataBaseManager.shared.saveContext()
    }
}

extension CreateGoalScenario: IterableVCDelegate {
    
    func didPressSkipButton(typeValueVC: IterableVC!) {
        check(typeValueVC: typeValueVC)
    }
    
    func didPressCancelButton(typeValueVC: IterableVC!) {
        delegate?.didCancel(createGoalScenario: self)
    }
    
    func didPressNextButton(typeValueVC: IterableVC!) {
        check(typeValueVC: typeValueVC)
    }
    
    func check(typeValueVC: IterableVC!) {
        if typeValueVC == nameVC {
            showTypeAmountVC()
        } else if typeValueVC == amountVC {
            showPhotoVC()
        } else if typeValueVC == photoVC {
            showDatePickerVC()
        } else if typeValueVC == datePickerVC {
            createGoal()
            delegate?.didComplete(createGoalScenario: self)
        }
    }
}
