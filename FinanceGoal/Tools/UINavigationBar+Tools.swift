//
//  UINavigationBar+Tools.swift
//  FinanceGoals
//
//  Created by Ilya Bondarenko on 1/24/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationBar {
    
    func add(button: UIButton!) {
        addSubview(button)
        let trailingContraint = NSLayoutConstraint(item: button, attribute:
            .trailingMargin, relatedBy: .equal, toItem: self,
                             attribute: .trailingMargin, multiplier: 1.0, constant: -20)
        let bottomConstraint = NSLayoutConstraint(item: button, attribute: .bottom, relatedBy: .equal,
                                                  toItem: self, attribute: .bottom, multiplier: 1.0, constant: -13.0)
        button.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([trailingContraint, bottomConstraint])
    }
    
    func defaultSetup() {
        setBackgroundImage(UIImage(), for: .default)
        shadowImage = UIImage()
        isTranslucent = true
        backgroundColor = .clear
        barTintColor = .black
        barStyle = .default
    }
    
    func clear() {
        setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        shadowImage = UIImage()
        isTranslucent = true
        tintColor = .white
        backgroundColor = .clear
        barStyle = .black
        UIApplication.shared.statusBarView?.backgroundColor = .clear
    }
}

extension UINavigationBar {
    
    func showBorderLine() {
        findBorderLine().isHidden = false
    }
    
    func hideBorderLine() {
        findBorderLine().isHidden = true
    }
    
    private func findBorderLine() -> UIImageView! {
        return self.subviews
            .flatMap { $0.subviews }
            .flatMap { $0 as? UIImageView }
            .filter { $0.bounds.size.width == self.bounds.size.width }
            .filter { $0.bounds.size.height <= 2 }
            .first
    }
}
