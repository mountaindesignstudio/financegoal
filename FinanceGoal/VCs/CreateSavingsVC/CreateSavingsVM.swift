//
//  CreateSavingsVM.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/30/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class CreateSavingsVM: KViewModel {

    private(set) var goal: Goal?
    
    /// The delegate object value for cell's callbacks
    weak var cellDelegate: CreateSavingsVCDelegate?
    
    /// The current amount value
    private(set) var amountValue: String! = "0.0"
    
    /// The current selected transaction type
    private(set) var transactionType: TransactionType! = .save
    
    // MARK: - Init method
    
    init(goal: Goal?, amountValue: String?, cellDelegate: CreateSavingsVCDelegate?, transactionType: TransactionType!) {
        super.init()
        
        self.goal = goal
        self.amountValue = amountValue ?? ""
        self.cellDelegate = cellDelegate
        self.transactionType = transactionType
        
        generateSections()
    }
    
    // MARK: - Private Tools
    
    private func generateSections() {
        
        var cellModels = [KCellModel]()
        
        cellModels.append(marginCell(height: 40.0))
        cellModels.append(cellModel(withTitle: "GOAL".localized.uppercased()))
        cellModels.append(cellModelForGoal())
        cellModels.append(marginCell(height: 20.0))
        cellModels.append(cellModel(withTitle: "AMOUNT".localized.uppercased()))
        cellModels.append(cellModelForAmount())
        
        sections.append(KSectionModel(cellModels: cellModels))
    }
    
    func cellModel(withTitle title: String!) -> KCellModel {
        let cm = TitleCellModel(titleValue: title, cellID: TableCellID.titleTableCell, height: 30.0)
        return cm
    }
    
    func cellModelForGoal() -> KCellModel {
        let cmd = ChangeGoalCommand(delegate: cellDelegate)
        let cm = SelectGoalCellModel(cellID: TableCellID.selectGoalTableCell, height: 60.0, goal: goal, command: cmd)
        return cm
    }
    
    func cellModelForAmount() -> KCellModel {
        let cm = TypeAmountCellModel(value: amountValue, placeholder: "0.0")
        cm.typeCommand = TypeAmountCommand(delegate: cellDelegate)
        return cm
    }
}

extension CreateSavingsVM {
    
    var completeButtonTitleValue: String! {
        return transactionTypeText + " " + amountValue.price
    }
    
    var isEnableCompleteButton: Bool! {
        return amountValue.price.count > 0
    }
    
    private var transactionTypeText: String! {
        switch transactionType {
        case .take?:
            return "TAKE".localized
        case .save?:
            return "SAVE".localized
        default:
            return ""
        }
    }
}
