//
//  SelectValueVC.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 2/13/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

protocol SelectValueVCDelegate: class {
    
    /// This method shoud be calling when the SelectValueVC did load
    func viewDidLoad(selectValueVC: SelectValueVC!)
    
    func didSelect(value: Any?)
}

class SelectValueVC: KTableView {

    @IBOutlet weak var _tableView: UITableView!
    
    override var tableView: UITableView! {
        return _tableView
    }
    
    var delegate: SelectValueVCDelegate?
    
    // MARK: - Init method
    
    init() {
        super.init(nibName: "SelectValueVC", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate?.viewDidLoad(selectValueVC: self)
    }
}
