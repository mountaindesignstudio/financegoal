//
//  TypeAmountTableCell.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/31/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class TypeAmountTableCell: KTableCell {

    @IBOutlet weak var typeField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        typeField.delegate = self
        typeField.becomeFirstResponder()
    }
    
    override func configure(cellModel: KCellModel!) {
        super.configure(cellModel: cellModel)
        
        guard let cm = cm else { return }
        typeField.placeholder = cm.placeholderValue
        typeField.text = cm.priceValue
        typeField.becomeFirstResponder()
    }
    
}

extension TypeAmountTableCell {
    
    var cm: TypeAmountCellModel? {
        return cellModel as? TypeAmountCellModel
    }
}

extension TypeAmountTableCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let cm = cm else { return false }
        
        switch string {
        case "0","1","2","3","4","5","6","7","8","9":
            cm.value += string
            textField.text = cm.value.price
        case ".", ",":
            if !cm.value.contains(".") || !cm.value.contains(".") {
                cm.value += string
                textField.text = cm.value.price
            }
        default:
            let array = Array(string)
            var currentStringArray = Array(cm.value)
            if array.count == 0 && currentStringArray.count != 0 {
                currentStringArray.removeLast()
                cm.value = ""
                for character in currentStringArray {
                    cm.value += String(character)
                }
                
                textField.text = cm.value.price
            }
        }
        
        return false
    }
}
