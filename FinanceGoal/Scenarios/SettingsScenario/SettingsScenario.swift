//
//  SettingsScenario.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 12/26/18.
//  Copyright © 2018 Ilya Bondarenko. All rights reserved.
//

import UIKit

class SettingsScenario: TabBarItemScenario, ScenarioInterface {

    private(set) var settingsVC: SettingsVC?
    
    override var titleValue: String! {
        return "SETTINGS".localized
    }
    
    // MARK: - Init method
    
    override init() {
        super.init()
        
        createSettingsVC()
    }
    
    // MARK: - ScenarioInterface
    
    func start() {
        
    }
    
    func stop() {
        
    }
    
    // MARK: - Private tools
    
    private func createSettingsVC() {
        settingsVC = SettingsVC()
        createNavigationController(forViewController: settingsVC!)
    }
}
