//
//  KTableView.swift
//  FinanceGoals
//
//  Created by Ilya Bondarenko on 10/26/18.
//  Copyright © 2018 Ilya Bondarenko. All rights reserved.
//

import UIKit

class KTableView: KeyboardVC, KTableDataSourceDelegate {
    
    /// The table view object for show data.
    private(set) var tableView: UITableView!
    
    /// The table view bottom margin constraint for update bottom margin if keyboard is shown.
    private(set) var tableViewBottomMargin: NSLayoutConstraint?
    
    /// Data source which contains data for table view. (Not UITableViewDataSource)
    private(set) var dataSource: KTableDataSource?
    
    /// The model view for Table View Controller.
    private(set) var viewModel: KViewModel?
    
    // MARK: - Tools
    
    func update(viewModel: KViewModel!) {
        self.viewModel = viewModel
        
        guard let vm = self.viewModel else { return }
        dataSource = KTableDataSource(sections: vm.sections)
        dataSource?.delegate = self
        tableView?.delegate = dataSource
        tableView?.dataSource = dataSource
        registerCell()
        tableView?.reloadData()
    }
    
    // MARK: - Private Tools
    
    func registerCell() {
        for sm in dataSource!.sections {
            for cm in sm.cellModels {
                tableView?.register(UINib(nibName: cm.cellID, bundle: nil),
                                   forCellReuseIdentifier: cm.cellID)
            }
        }
    }
    
    // MARK: - KTableDataSourceDelegate methods
    
    func didScroll(dataSource: KTableDataSource!) { /** Should override in subclass */ }
}
