//
//  GoalLargeTableCell.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/22/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit
import SDWebImage

class GoalLargeTableCell: KTableCell {

    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var dataContainer: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var progressViewWidth: NSLayoutConstraint!
    
    // MARK: -
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.layer.cornerRadius = 16.0
        progressView.layer.cornerRadius = 2.0
        blurView.addBlur(.extraLight)
        dataContainer.backgroundColor = .clear
        shadowView.layer.cornerRadius = 16.0
        shadowView.addShadow(offsetHeight: 0.0)
    }
    
    override func configure(cellModel: KCellModel!) {
        super.configure(cellModel: cellModel)
        
        titleLabel.text = cm?.title
        progressLabel.text = cm?.percentValue
        progressLabel.sizeToFit()
        changeProgessWidth(withProgress: cm?.progress ?? 0.0)
        
        photoView.sd_setImage(with: cm!.imageURL) { (image, error, type, url) in
            self.photoView.image = image
        }
    }
}

extension GoalLargeTableCell {
    
    var cm: GoalCellModel? {
        return cellModel as? GoalCellModel
    }
}

extension GoalLargeTableCell {
    
    func changeProgessWidth(withProgress progress: Float!) {
        let sideMargin: CGFloat = 39.0*2+10
        let labelWidth = progressLabel.frame.width
        let availableWidth = UIScreen.main.bounds.width - sideMargin - labelWidth
        progressViewWidth.constant = availableWidth*CGFloat(progress)
    }
}
