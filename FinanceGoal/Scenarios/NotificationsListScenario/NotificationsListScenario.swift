//
//  NotificationsListScenario.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 12/26/18.
//  Copyright © 2018 Ilya Bondarenko. All rights reserved.
//

import UIKit

class NotificationsListScenario: TabBarItemScenario, ScenarioInterface {

    private(set) var notificationsListVC: NotificationsListVC?
    
    override var titleValue: String! {
        return "NOTIFICATIONS".localized
    }
    
    // MARK: - Init method
    
    override init() {
        super.init()
        
        createNotificationsListVC()
    }
    
    // MARK: - ScenarioInterface
    
    func start() {
        
    }
    
    func stop() {
        
    }
    
    // MARK: - Private tools
    
    private func createNotificationsListVC() {
        notificationsListVC = NotificationsListVC()
        createNavigationController(forViewController: notificationsListVC!)
    }
}
