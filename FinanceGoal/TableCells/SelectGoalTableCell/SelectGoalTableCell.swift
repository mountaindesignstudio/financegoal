//
//  SelectGoalTableCell.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/30/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class SelectGoalTableCell: KTableCell {

    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .default
        
        photoImageView.layer.cornerRadius = 8.0
        photoImageView.clipsToBounds = true
    }
    
    override func configure(cellModel: KCellModel!) {
        super.configure(cellModel: cellModel)
        
        guard let cm = cm else { return }
        
        titleLabel.text = cm.title
        amountLabel.text = cm.amount
        photoImageView.sd_setImage(with: cm.imageURL) { (image, error, type, url) in
            self.photoImageView.image = image
        }
    }
}

extension SelectGoalTableCell {
    
    var cm: SelectGoalCellModel? {
        return cellModel as? SelectGoalCellModel
    }
}
