//
//  GoalDetailsTableCell.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/26/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class GoalDetailsTableCell: KTableCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var costValueLabel: UILabel!
    @IBOutlet weak var statusValueLabel: UILabel!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var progressViewWidth: NSLayoutConstraint!
    @IBOutlet weak var leftLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        progressView.layer.cornerRadius = 2.0
        
        progressView.backgroundColor = Colors.tint
        progressLabel.textColor = Colors.tint
        statusValueLabel.textColor = Colors.tint
        leftLabel.textColor = #colorLiteral(red: 0.7710450292, green: 0.7676748633, blue: 0.7675936222, alpha: 1)
        costValueLabel.textColor = Colors.tint
        costValueLabel.layer.opacity = 0.5
    }
    
    override func configure(cellModel: KCellModel!) {
        super.configure(cellModel: cellModel)
        
        guard let cm = cm else { return }
        
        titleLabel.text = cm.title
        statusValueLabel.text = cm.statusValue
        costValueLabel.text = "OF".localized + " " + cm.costValue
        progressLabel.text = cm.percentValue
        progressLabel.sizeToFit()
        changeProgessWidth(withProgress: cm.progress)
        leftLabel.text = cm.leftValue
    }
}

extension GoalDetailsTableCell {
    
    var cm: GoalDetailsCellModel? {
        return cellModel as? GoalDetailsCellModel
    }
}

extension GoalDetailsTableCell {
    
    func changeProgessWidth(withProgress progress: Float!) {
        let sideMargin: CGFloat = 24.0*2+10
        let labelWidth = progressLabel.frame.width
        let availableWidth = UIScreen.main.bounds.width - sideMargin - labelWidth
        progressViewWidth.constant = availableWidth*CGFloat(progress)
    }
}
