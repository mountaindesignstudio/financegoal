//
//  GoalListResultController.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 2/17/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit
import CoreData

protocol GoalListResultControllerDelegate: class {
    
}

class GoalListResultController: NSFetchedResultsController<NSFetchRequestResult> {

    static func controller(withKeyForSort sortKey: String?) -> GoalListResultController {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: EntityName.goal)
        if let key = sortKey {
            let sortDescriptor = NSSortDescriptor(key: key, ascending: true)
            fetchRequest.sortDescriptors = [sortDescriptor]
        }
        
        let fetchedResultsController = GoalListResultController(fetchRequest: fetchRequest,
                                                                managedObjectContext: DataBaseManager.shared.managedObjectContext,
                                                                sectionNameKeyPath: nil,
                                                                cacheName: nil)
        return fetchedResultsController
    }
    
    var resultControllerDelegate: GoalListResultControllerDelegate?
    
    override init(fetchRequest: NSFetchRequest<NSFetchRequestResult>, managedObjectContext context: NSManagedObjectContext, sectionNameKeyPath: String?, cacheName name: String?) {
        super.init(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: sectionNameKeyPath, cacheName: name)
        delegate = self
    }
}

extension GoalListResultController: NSFetchedResultsControllerDelegate {
    
}
