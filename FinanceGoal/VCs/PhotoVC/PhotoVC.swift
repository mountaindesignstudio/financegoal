//
//  PhotoVC.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/21/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class PhotoVC: IterableVC {

    @IBOutlet weak var counterContainerView: UIView!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var titleTextLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageActionButton: UIButton!
    
    private(set) var picker: UIImagePickerController?
    
    lazy var skipButton: UIBarButtonItem = { [unowned self] in
        return UIBarButtonItem(title: "SKIP".localized, style: .done, target: self, action: #selector(PhotoVC.didPressSkipButton))
        }()

    override var _counterContainerView: UIView! {
        return counterContainerView
    }
    
    override var _counterLabel: UILabel! {
        return counterLabel
    }
    
    override var _titleTextLabel: UILabel! {
        return titleTextLabel
    }
    
    override var _descriptionLabel: UILabel! {
        return descriptionLabel
    }
    
    init() {
        super.init(nibName: "PhotoVC", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setupViews() {
        super.setupViews()
        
        imageView.layer.cornerRadius = 8.0
        imageView.layer.borderColor = Colors.tint.cgColor
        imageView.layer.borderWidth = 2.0
        imageView.clipsToBounds = true
    }
    
    override func updateData() {
        super.updateData()
        
        self.imageView.image = vm?.image
        
        let buttonImage: UIImage? = vm?.image != nil ? nil : #imageLiteral(resourceName: "large_add_icon")
        imageActionButton.setImage(buttonImage, for: .normal)
        
        updateBarButtons()
    }
    
    override func updateBarButtons() {
        navigationController?.navigationBar.tintColor = Colors.tint
        
        if let _ = vm?.image {
            navigationItem.rightBarButtonItem = nextButton
        } else {
            navigationItem.rightBarButtonItem = skipButton
        }
    }
    
    @IBAction func imageAction(_ sender: Any) {
        showPhotoActionSheet()
    }
}

extension PhotoVC {
    
    var vm: PhotoViewModel? {
        return viewModel as? PhotoViewModel
    }
}

extension PhotoVC {
    
    @objc func didPressSkipButton() {
        delegate?.didPressSkipButton(typeValueVC: self)
    }
}

extension PhotoVC: UIImagePickerControllerDelegate, UIPopoverControllerDelegate, UINavigationControllerDelegate {
    
    // MARK: - Private tools
    
    private func showPhotoActionSheet() {
        
        let actionSheetVC: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheetVC.view.tintColor = Colors.tint
        
        let cancelActionButton = UIAlertAction(title: "CANCEL".localized, style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetVC.addAction(cancelActionButton)
        
        let camera = UIAlertAction(title: "CAMERA".localized, style: .default)
        { _ in
            self.openCamera()
        }
        actionSheetVC.addAction(camera)
        
        let photos = UIAlertAction(title: "PHOTOS".localized, style: .default)
        { _ in
            self.openGallary()
        }
        actionSheetVC.addAction(photos)
        
        if imageView.image != nil {
            let delete = UIAlertAction(title: "DELETE".localized, style: .destructive)
            { _ in
                self.vm?.image = nil
                self.updateData()
            }
            actionSheetVC.addAction(delete)
        }
        
        present(actionSheetVC, animated: true, completion: nil)
    }
    
    func openGallary()
    {
        picker = UIImagePickerController()
        picker?.delegate = self
        picker?.view.tintColor = Colors.tint
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerController.SourceType.photoLibrary
        present(picker!, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        picker = UIImagePickerController()
        picker?.delegate = self
        picker?.view.tintColor = Colors.tint
        
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker!.allowsEditing = false
            picker!.sourceType = UIImagePickerController.SourceType.camera
            picker!.cameraCaptureMode = .photo
            present(picker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let chosenImage: UIImage? = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        let chosenImageURL: URL? = info[UIImagePickerController.InfoKey.imageURL] as? URL
        self.vm?.image = chosenImage
        self.vm?.imageURL = chosenImageURL
        self.updateData()
        picker.dismiss(animated: true, completion: nil)
    }
}
