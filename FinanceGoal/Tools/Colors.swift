//
//  Colors.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 12/29/18.
//  Copyright © 2018 Ilya Bondarenko. All rights reserved.
//

import Foundation
import UIKit

struct Colors {
    static let tint = #colorLiteral(red: 0.9764705882, green: 0.5019607843, blue: 0.2431372549, alpha: 1)
}
