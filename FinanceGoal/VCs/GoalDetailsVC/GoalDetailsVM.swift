//
//  GoalDetailsVM.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/25/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class GoalDetailsVM: KViewModel {

    private(set) var goal: Goal!
    
    // MARK: - Init method
    
    init(goal: Goal!) {
        super.init()
        
        self.goal = goal
        generateSection()
    }
    
    var image: UIImage? {
        guard let data = goal?.image else { return nil }
        return UIImage(data: data)
    }
    
    var imageURL: URL? {
        return goal?.imageURL
    }
    
    // MARK: - Private Tools
    
    private func generateSection() {
        
        var cellModels = [KCellModel]()
        let height = UIScreen.main.bounds.width*0.84
        cellModels.append(KMarginCellModel(height: height))
        cellModels.append(goalDetailsCM())
        cellModels.append(KMarginCellModel(height: height))
        cellModels.append(KMarginCellModel(height: height))
        cellModels.append(KMarginCellModel(height: height))
        
        sections.append(KSectionModel(cellModels: cellModels))
    }
    
    func goalDetailsCM() -> GoalDetailsCellModel {
        let cm = GoalDetailsCellModel(title: goal.name ?? "",
                                      costValue: String(goal.amount).price,
                                      statusValue: String(goal.savingsAmount).price,
                                      progress: goal.progress,
                                      leftValue: "left".localized + " " + String(goal.leftAmount).price,
                                      cellID: TableCellID.goalDetailsTableCell,
                                      height: 160.0)
        cm.update(separatorInset: UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24))
        return cm
    }
}
