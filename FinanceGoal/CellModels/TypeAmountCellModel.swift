//
//  TypeAmountCellModel.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/31/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class TypeAmountCellModel: KCellModel {

    private var _value: String! = ""
    
    var value: String! {
        get {
            return _value
        }
        set {
            _value = newValue
            typeCommand?.execute?(withValue: _value)
        }
    }
    
    private(set) var placeholder: String! = ""
    
    var typeCommand: KCellCommandProtocol?
    
    init(value: String!, placeholder: String!) {
        super.init(cellID: TableCellID.typeAmountTableCell, height: 80.0)
        
        self.value = value
        self.placeholder = placeholder
        
        update(separatorInset: UIEdgeInsets(top: 0.0, left: 20.0, bottom: 0.0, right: 20.0))
    }
    
    var priceValue: String! {
        return value.count > 0 ? value.price : ""
    }
    
    var placeholderValue: String! {
        return placeholder.count > 0 ? placeholder.price : ""
    }
}
