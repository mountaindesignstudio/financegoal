//
//  TabBarItemScenario.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 12/26/18.
//  Copyright © 2018 Ilya Bondarenko. All rights reserved.
//

import UIKit

class TabBarItemScenario: NavigationScenario {
    
    /// Current tab bar item for tab bar controller
    var tabBarItem: UITabBarItem! {
        let notSelectedIcon: UIImage? = nil
        let selectedIcon: UIImage? = nil
        let tabBarItem = UITabBarItem(title: titleValue, image: notSelectedIcon, selectedImage: selectedIcon)
        tabBarItem.tag = 0
        return tabBarItem
    }
    
    // MARK: - Tools
    
    override func createNavigationController(forViewController viewController: UIViewController!) {
        super.createNavigationController(forViewController: viewController)
        nvc?.tabBarItem = tabBarItem
    }
}
