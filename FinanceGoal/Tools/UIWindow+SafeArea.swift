//
//  UIWindow+SafeArea.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 2/11/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import Foundation
import UIKit

struct SafeArea {
    static let top = UIWindow.currentWindow?.safeAreaInsets.top ?? 0.0
    static let bottom = UIWindow.currentWindow?.safeAreaInsets.bottom ?? 0.0
}

extension UIWindow {
    static var currentWindow: UIWindow? {
        return UIApplication.shared.keyWindow
    }
}
