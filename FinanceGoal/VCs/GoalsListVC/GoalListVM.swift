//
//  GoalListVM.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/22/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class GoalListVM: KViewModel {

    private(set) var goals: [Goal]! = [Goal]()
    private(set) var isEmpty: Bool! = true
    weak var commandDelegate: GoalsListVCDelegate?
    
    // MARK: - Init method
    
    init(goals: [Goal]!, delegate: GoalsListVCDelegate?) {
        super.init()
        
        self.goals = goals
        self.isEmpty = !(goals.count > 0)
        self.commandDelegate = delegate
        
        generateSection()
    }
    
    // MARK: - Private Tools
    
    private func generateSection() {
        
        var cellModels = [KCellModel]()
        cellModels.append(KMarginCellModel(height: 11.0))
        for goal in goals {
            let height = (UIScreen.main.bounds.width - 40.0)/1.3
            let cmd = SelectGoalCommand(delegate: commandDelegate)
            let cm = GoalCellModel(cellID: TableCellID.goalLargeCell, height: height, goal: goal, command: cmd)
            cellModels.append(cm)
        }
        
        sections.append(KSectionModel(cellModels: cellModels))
    }
}
