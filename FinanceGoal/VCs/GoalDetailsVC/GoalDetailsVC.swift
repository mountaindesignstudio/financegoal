//
//  GoalDetailsVC.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/24/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

protocol GoalDetailsVCDelegate: class {
    
    /// This methid shoud be calling when the Goal Details vc did load
    func viewDidLoad(goalDetailsVC: GoalDetailsVC!)
}

/// KTableView
class GoalDetailsVC: KTableView {
    
    var delegate: GoalDetailsVCDelegate?
    
    var currentBarStyle: UIStatusBarStyle! = .lightContent
    
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var photoViewHeight: NSLayoutConstraint!
    @IBOutlet weak var photoViewTopMargin: NSLayoutConstraint!
    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var navigationBarBackground: UIView!
    @IBOutlet weak var navBarBackgorundHeight: NSLayoutConstraint!
    
    @IBOutlet weak var _tableView: UITableView!
    
    override var tableView: UITableView! {
        return _tableView
    }
    
    var defaultPhotoHeight: CGFloat! {
        return UIScreen.main.bounds.width*0.85
    }
    
    init() {
        super.init(nibName: "GoalDetailsVC", bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNeedsStatusBarAppearanceUpdate()
        
        let navigationBarHeight = navigationController?.navigationBar.frame.size.height ?? 0.0
        let statusBarHeight = UIApplication.shared.statusBarFrame.size.height
        
        tableView.contentInset = UIEdgeInsets(top: -(navigationBarHeight+statusBarHeight),left: 0,bottom: 0,right: 0);
        photoViewHeight.constant = defaultPhotoHeight
        updateStatusBar(withBarStyle: .lightContent)
        delegate?.viewDidLoad(goalDetailsVC: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.clear()
        let statusBarHeight = UIApplication.shared.statusBarFrame.size.height
        navBarBackgorundHeight.constant = statusBarHeight + 44.0
        scrollEngineering()
    }
    
    override func update(viewModel: KViewModel!) {
        super.update(viewModel: viewModel)
        
        guard let vm = self.vm else { return }
        
        photoView.sd_setImage(with: vm.imageURL) { (image, error, type, url) in
            self.photoView.image = image
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return currentBarStyle
    }
    
    override func didScroll(dataSource: KTableDataSource!) {
        scrollEngineering()
    }
    
    func updateStatusBar(withBarStyle barStyle: UIStatusBarStyle!) {
        UIApplication.shared.statusBarStyle = barStyle
        setNeedsStatusBarAppearanceUpdate()
    }
}

extension GoalDetailsVC {
    
    var vm: GoalDetailsVM? {
        return viewModel as? GoalDetailsVM
    }
}

extension GoalDetailsVC {
    
    func scrollEngineering() {
        guard let tv = tableView else { return }
        changeImageSizeIfNeeded(offsetY: tv.contentOffset.y)
        showWhiteViewIfNeeded(offsetY: tv.contentOffset.y)
    }
    
    private func changeImageSizeIfNeeded(offsetY: CGFloat!) {
        if offsetY < 0.0 {
            photoViewTopMargin.constant = 0.0
            photoViewHeight.constant = (defaultPhotoHeight)+(-offsetY)
        } else if offsetY > 0.0 {
            photoViewHeight.constant = defaultPhotoHeight
            photoViewTopMargin.constant = -(offsetY/7.0)
        } else {
            photoViewTopMargin.constant = 0.0
            photoViewHeight.constant = defaultPhotoHeight
        }
    }
    
    private func showWhiteViewIfNeeded(offsetY: CGFloat!) {
        
        let statusBarHeight = UIApplication.shared.statusBarFrame.size.height
        let margin: CGFloat = defaultPhotoHeight - (statusBarHeight+44.0) - offsetY
        let validMargin: CGFloat = 100.0
        
        if margin < validMargin {
            let value: CGFloat = validMargin - margin
            whiteView.layer.opacity = Float(value/validMargin)
            updateNavBarIfNeeded(opacity: whiteView.layer.opacity)
        } else {
            whiteView.layer.opacity = 0.0
        }
    }
    
    private func updateNavBarIfNeeded(opacity: Float!) {
        if whiteView.layer.opacity <= 0.6 {
            updateStatusBar(withBarStyle: .lightContent)
            navigationController?.navigationBar.tintColor = .white
        } else {
            updateStatusBar(withBarStyle: .default)
            navigationController?.navigationBar.tintColor = Colors.tint
        }
        
        if whiteView.layer.opacity >= 1.0 {
            navigationBarBackground.isHidden = false
//            UIApplication.shared.statusBarView?.backgroundColor = .white
        } else {
            navigationBarBackground.isHidden = true
//            UIApplication.shared.statusBarView?.backgroundColor = .clear
        }
    }
}
