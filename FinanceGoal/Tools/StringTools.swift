//
//  StringTools.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 12/26/18.
//  Copyright © 2018 Ilya Bondarenko. All rights reserved.
//

import Foundation

extension String {
    
    /// Return string value from localizable file
    var localized: String {
        let value:String = NSLocalizedString(self, comment: "")
        return value
    }
    
    var price: String {
        let nf = NumberFormatter()
        nf.numberStyle = .currency
        
        let number: NSDecimalNumber = NSDecimalNumber(string: self)
        if number.doubleValue.isNaN {
            return ""
        }
        
        return nf.string(from: number) ?? ""
    }
    
    var priceWithoutCurrency: String {
        let nf = NumberFormatter()
        nf.numberStyle = .currency
        nf.currencySymbol = ""
        
        let number: NSDecimalNumber = NSDecimalNumber(string: self)
        if number.doubleValue.isNaN {
            return ""
        }
        
        return nf.string(from: number) ?? ""
    }
    
    static func generateID(withKey key: String! = "") -> String {
        let date = Date()
        let rand = Int(date.timeIntervalSince1970)
        return key + String(rand)
    }
}
