//
//  SelectGoalCellModel.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/30/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class SelectGoalCellModel: GoalCellModel {

    override init(cellID: CellID!, height: CGFloat!, goal: Goal!, command: KCellCommandProtocol?) {
        super.init(cellID: cellID, height: height, goal: goal, command: command)
        
        update(separatorInset: UIEdgeInsets(top: 0, left: 82.0, bottom: 0, right: 20.0))
    }
    
    override var title: String! {
        return super.title.count > 0 ? super.title : "SELECT_GOAL".localized
    }
}
