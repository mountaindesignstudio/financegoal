//
//  KTableDataSource.swift
//  FinanceGoals
//
//  Created by Ilya Bondarenko on 10/26/18.
//  Copyright © 2018 Ilya Bondarenko. All rights reserved.
//

import UIKit

@objc protocol KTableDataSourceDelegate: class {
    
    @objc optional func didScroll(dataSource: KTableDataSource!)
}

class KTableDataSource: NSObject {

    weak var delegate: KTableDataSourceDelegate?
    
    /// Array of Section models for draw sections in table view.
    private(set) var sections: [KSectionModel]! = [KSectionModel]()
    
    // MARK: - Init method
    
    init(sections: [KSectionModel]!) {
        super.init()
        
        self.sections = sections
    }
}

extension KTableDataSource: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = sections[section]
        return section.cellModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section: KSectionModel = sections[indexPath.section]
        let cellModel: KCellModel = section.cellModels[indexPath.row]
        let cell: KTableCell = tableView.dequeueReusableCell(withIdentifier: cellModel.cellID, for: indexPath) as! KTableCell
        cell.configure(cellModel: cellModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? KTableCell else { return }
        cell.select()
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension KTableDataSource: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section: KSectionModel = sections[indexPath.section]
        let cellModel: KCellModel = section.cellModels[indexPath.row]
        return cellModel.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let section: KSectionModel = sections[indexPath.section]
        let cellModel: KCellModel = section.cellModels[indexPath.row]
        return cellModel.height
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        delegate?.didScroll?(dataSource: self)
    }
}
