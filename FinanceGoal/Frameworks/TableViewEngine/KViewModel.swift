//
//  KViewModel.swift
//  FinanceGoals
//
//  Created by Ilya Bondarenko on 10/26/18.
//  Copyright © 2018 Ilya Bondarenko. All rights reserved.
//

import UIKit

class KViewModel: NSObject {

    /// Array of Section models for draw sections in table view.
    var sections: [KSectionModel]! = [KSectionModel]()
    
    /// This method return margin cell model for Margin Table Cell.
    /// This cell has clear backgorund color and can use for add margin for cells.
    func marginCell(height: CGFloat!) -> KMarginCellModel {
        return KMarginCellModel(height: height)
    }
}
