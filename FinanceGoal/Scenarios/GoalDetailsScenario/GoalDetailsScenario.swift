//
//  GoalDetailsScenario.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/24/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

protocol GoalDetailsScenarioDelegate: class {
    
}

class GoalDetailsScenario: NavigationScenario, ScenarioInterface {
    
    /// Delegate object for callbacks
    weak var delegate: GoalDetailsScenarioDelegate?
    
    /// The goal object
    private var goal: Goal!
    
    /// The Goal Details view controller
    private var goalDetailsVC: GoalDetailsVC?
    
    // MARK: - Init method
    
    init(goal: Goal!, nvc: UINavigationController!) {
        super.init()
        
        self.goal = goal
        self.nvc = nvc
    }
    
    // MARK: - ScenarioInterface methods
    
    func start() {
        showGoalDetailsScreen()
    }
    
    func stop() {}
    
    // MARK: -
    
    private func showGoalDetailsScreen() {
        goalDetailsVC = GoalDetailsVC()
        goalDetailsVC?.delegate = self
        goalDetailsVC?.hidesBottomBarWhenPushed = true
        nvc?.pushViewController(goalDetailsVC!, animated: true)
    }
    
    // MARK: - Tools
    
    func reloadData() {
        let vm = GoalDetailsVM(goal: goal)
        goalDetailsVC?.update(viewModel: vm)
    }
}

extension GoalDetailsScenario: GoalDetailsVCDelegate {
    
    func viewDidLoad(goalDetailsVC: GoalDetailsVC!) {
        reloadData()
    }
}
