//
//  KCellModel.swift
//  FinanceGoals
//
//  Created by Ilya Bondarenko on 10/26/18.
//  Copyright © 2018 Ilya Bondarenko. All rights reserved.
//

import UIKit

typealias CellID = String

class KCellModel: NSObject {

    /// UITableViewCell's identifer for load XIB file and register cell in Table View.
    private(set) var cellID: CellID!
    
    /// UITableViewCell's height value.
    /// By default this property equal to automatic dimension.
    private(set) var height: CGFloat! = UITableView.automaticDimension
    
    /// UITableViewCell's separator inset.
    /// By default equal to zero.
    private(set) var separatorInset: UIEdgeInsets! = UIEdgeInsets.zero
    
    /// Action command for cell selection
    private(set) var selectAction: KCellCommandProtocol?
    
    // MARK: - Init method
    
    init(cellID: CellID!, height: CGFloat!, selectAction: KCellCommandProtocol? = nil) {
        super.init()
        
        self.cellID = cellID
        self.height = height
        self.selectAction = selectAction
    }
    
    func select() {
        selectAction?.execute?()
        /// Must be override in subclass if needed add custom logic for selection
    }
    
    // MARK: - Update methods
    
    func update(separatorInset: UIEdgeInsets!) {
        self.separatorInset = separatorInset
    }
}

/// K Cell Command Protocol
@objc protocol KCellCommandProtocol {
    
    /// This method execute command without any data.
    @objc optional func execute()
    
    /// This method execute command with any data.
    @objc optional func execute(withValue value: Any?)
}
