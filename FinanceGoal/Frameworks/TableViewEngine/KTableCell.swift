//
//  KTableCell.swift
//  FinanceGoals
//
//  Created by Ilya Bondarenko on 10/26/18.
//  Copyright © 2018 Ilya Bondarenko. All rights reserved.
//

import UIKit

class KTableCell: UITableViewCell {
    
    /// The cell model for current table cell.
    private(set) var cellModel: KCellModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
    }
    
    // MARK: - Tools
    
    func configure(cellModel: KCellModel!) {
        self.cellModel = cellModel
        self.separatorInset = cellModel.separatorInset
    }
    
    func select() {
        cellModel?.select()
        /** Must be override in subclass */
    }
}
