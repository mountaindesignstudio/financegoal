//
//  GoalCellModel.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/22/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class GoalCellModel: KCellModel {

    private(set) var goal: Goal!
    
    // MARK: - Init method
    
    init(cellID: CellID!, height: CGFloat!, goal: Goal!, command: KCellCommandProtocol?) {
        super.init(cellID: cellID, height: height, selectAction: command)
        
        self.goal = goal
        update(separatorInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: UIScreen.main.bounds.width))
    }
    
    var title: String! {
        return goal.name ?? ""
    }
    
    var percentValue: String! {
        return String(Int(goal.progress*100)) + "%"
    }
    
    var progress: Float {
        return goal.progress
    }
    
    var left: String {
        return "left".localized + " " + String(goal.leftAmount).price
    }
    
    var image: UIImage? {
        if let data = goal.image {
            return UIImage(data: data)
        }
        
        return nil
    }
    
    var imageURL: URL? {
        return goal.imageURL
    }
    
    var amount: String! {
        return String(goal.amount).price
    }
    
    override func select() {
        selectAction?.execute?(withValue: goal)
    }
}
