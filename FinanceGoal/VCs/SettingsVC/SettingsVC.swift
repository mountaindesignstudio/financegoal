//
//  SettingsVC.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 12/26/18.
//  Copyright © 2018 Ilya Bondarenko. All rights reserved.
//

import UIKit

protocol SettingsVCDelegate: class {
    
}

class SettingsVC: UIViewController {
    
    var delegate: SettingsVCDelegate?
    
    // MARK: - Init method
    
    init() {
        super.init(nibName: "SettingsVC", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
