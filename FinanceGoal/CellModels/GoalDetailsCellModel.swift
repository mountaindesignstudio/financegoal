//
//  GoalDetailsCellModel.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/26/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class GoalDetailsCellModel: KCellModel {
    
    var costTitle: String! { return "COST".localized + ":" }
    
    var statusTitle: String! { return "SAVED".localized + ":" }
    
    private(set) var title: String! = ""
    private(set) var costValue: String! = ""
    private(set) var statusValue: String! = ""
    private(set) var progress: Float! = 0.0
    private(set) var leftValue: String! = ""
    
    var percentValue: String! {
        let value: Int = Int(progress*100)
        return String(value)+"%"
    }
    
//    var leftValue: String! {
//        guard let cost = (costValue as NSString?)?.floatValue,
//            let status = (statusValue as NSString?)?.floatValue else { return "" }
//        return "left".localized + " " + String(cost - status).priceWithoutCurrency
//    }
    
    init(title: String!, costValue: String!, statusValue: String!, progress: Float!, leftValue: String!, cellID: CellID!, height: CGFloat) {
        super.init(cellID: cellID, height: height)
        
        self.title = title
        self.costValue = costValue
        self.statusValue = statusValue
        self.progress = progress
        self.leftValue = leftValue
    }
}
