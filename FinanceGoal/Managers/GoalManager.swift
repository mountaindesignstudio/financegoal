//
//  GoalManager.swift
//  FinanceGoal
//
//  Created by Ilya Bondarenko on 1/22/19.
//  Copyright © 2019 Ilya Bondarenko. All rights reserved.
//

import UIKit

class GoalManager: NSObject {

    static let shared = GoalManager()
    
    /// This method check if Goal is exists by ID and return it.
    /// If goal not exists the method returns new Goal.
    func goal(byGoalID goalID: String!) -> Goal {
        let predicate = NSPredicate(format: "goalID ==" + goalID)
        if let goal = DataBaseManager.shared.fetchEntities(forName: EntityName.goal, limit: 1, predicate: predicate).first as? Goal {
            return goal
        } else {
            let goal = DataBaseManager.shared.newEntity(name: EntityName.goal) as! Goal
            goal.goalID = goalID
            return goal
        }
    }
    
    /// This method fetch all goals and return it them in completion block.
    func fetchGoals(completion: GoalsCompletion) {
        if let goals = DataBaseManager.shared.fetchEntities(forName: EntityName.goal) as? [Goal] {
            completion?(goals)
        } else {
            completion?([Goal]())
        }
    }
}
